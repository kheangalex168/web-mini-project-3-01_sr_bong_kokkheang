import { Container, Form, Table, Button, Row, Col } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchCate,
  deleteCate,
  addCate,
  updateCate,
} from "../redux/action/categoryAction";
import strings from "../localization/localization";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const Category = () => {
  const [name, setName] = useState("");
  const [click, setClick] = useState("");
  const [id, setId] = useState("");
  const { category, message, did } = useSelector(
    (state) => state.categoryReducer
  );

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchCate());
    if (did) toast.success(message);
  }, [did]);

  const onAdd = async (e) => {
    e.preventDefault();
    let Author = {
      name,
    };
    dispatch(addCate(Author));
    setName("");
  };

  const onDelete = async (id) => {
    dispatch(deleteCate(id));
  };

  const onEdit = (id) => {
    setClick(true);
    setId(id);
    let tmp = category.filter((item) => {
      return item._id === id;
    });
    setName(tmp[0].name);
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let category = {
      name,
    };
    // updateAuthorById(id, Author).then((message) => alert(message));
    dispatch(updateCate(id, category));
    setClick(false);
    setName("");
  };

  return (
    <Container>
      <h1 className="my-3">{strings.category}</h1>
      <Col>
        <Form>
          <Form.Group controlId="category">
            <Row>
              <Form.Label>{strings.name}</Form.Label>
            </Row>
            <Row>
              <Col>
                <Row>
                  <Form.Control
                    type="text"
                    placeholder="Category Name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />
                </Row>
              </Col>
              <Col>
                <Button variant="secondary" onClick={click ? onUpdate : onAdd}>
                  {click ? "Save" : "Submit"}
                </Button>
              </Col>
            </Row>
          </Form.Group>
        </Form>
      </Col>

      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {category.map((item) => (
            <tr>
              <td>{item._id}</td>
              <td>{item.name}</td>
              <td>
                <Button
                  size="sm"
                  variant="warning"
                  onClick={() => onEdit(item._id)}
                >
                  Edit
                </Button>{" "}
                <Button
                  size="sm"
                  variant="danger"
                  onClick={() => onDelete(item._id)}
                >
                  Delete
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Container>
  );
};

export default Category;
