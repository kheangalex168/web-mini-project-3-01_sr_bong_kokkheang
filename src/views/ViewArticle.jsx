import React, { useState, useEffect } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { useParams } from "react-router";
import Comment from "../components/Comment";
import { useDispatch, useSelector } from "react-redux";
import { fetchArtByID } from "../redux/action/articleAction";
import { fetchArticleById } from "../services/article_service";

function ViewArticle() {
  const loader = {
    width: "100px",
  };
  const dispatch = useDispatch();
  const [article, setArticle] = useState();
  const { id } = useParams();
  // const { article, loading } = useSelector((state) => state.articleReducer);

  useEffect(() => {
    if (id) {
      fetchArticleById(id).then((article) => {
        setArticle(article);
        dispatch(fetchArtByID);
        // article.find((item) => (item._id = id));
        // console.log(article);
      });
    }
  }, []);

  return (
    <Container>
      {article ? (
        <>
          <h1 className="my-2">{article.title}</h1>
          <Row>
            <Col md={4}>
              <img
                className="w-100"
                src={
                  article.image
                    ? article.image
                    : "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"
                }
              />
              <Row>
                <Col md={8}>
                  <p>{article.description}</p>
                </Col>
              </Row>
            </Col>
            <Col>
              {/* <b>Comment</b>
              {loading ? ( */}
              <Comment />
              {/* ) : (
                <div className={loader}>
                  <ReactLoading type="cylon" color="blue" width="250px" />
                </div>
              )} */}
            </Col>
          </Row>
        </>
      ) : (
        ""
      )}
    </Container>
  );
}

export default ViewArticle;
