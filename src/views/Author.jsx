import React, { useState, useEffect } from "react";
import {
  Col,
  Container,
  Row,
  Form,
  Button,
  Table,
  Image,
} from "react-bootstrap";
import { uploadImage } from "../services/author_service";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchAu,
  deleteAu,
  addAu,
  updateAu,
} from "../redux/action/authorAction";
import strings from "../localization/localization";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function Author() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [imageURL, setImageURL] = useState(
    "https://designshack.net/wp-content/uploads/placeholder-image.png"
  );
  const [id, setId] = useState("");

  const [img, setImg] = useState("");

  const [click, setClick] = useState(false);

  const [imageFile, setImageFile] = useState(null);

  const dispatch = useDispatch();

  const { author, message, did } = useSelector((state) => state.authorReducer);

  useEffect(() => {
    dispatch(fetchAu());
    if (did) toast.success(message);
  }, [did]);

  const onAdd = async (e) => {
    e.preventDefault();
    let Author = {
      name,
      email,
    };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      Author.image = url;
    } else {
      Author.image = imageURL;
    }
    dispatch(addAu(Author));
    setName("");
    setEmail("");
    setImageURL(
      "https://designshack.net/wp-content/uploads/placeholder-image.png"
    );
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let Author = {
      name,
      email,
    };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      Author.image = url;
    }
    // updateAuthorById(id, Author).then((message) => alert(message));
    dispatch(updateAu(id, Author));
    setClick(false);
    setName("");
    setEmail("");
    setImageURL(
      "https://designshack.net/wp-content/uploads/placeholder-image.png"
    );
  };

  const onDelete = async (id) => {
    dispatch(deleteAu(id));
  };

  const onEdit = (id) => {
    setClick(true);
    setId(id);
    let tmp = author.filter((item) => {
      return item._id === id;
    });
    setName(tmp[0].name);
    setEmail(tmp[0].email);
    setImg(tmp[0].image);
  };

  return (
    <Container>
      <Col>
        <h1>{strings.author}</h1>
      </Col>
      <br />
      <Col>
        <Row>
          <Col md={8}>
            <Form>
              <Form.Group controlId="title">
                <Form.Label>{strings.name}</Form.Label>
                <Form.Control
                  type="text"
                  placeholder={strings.name}
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>
              <Form.Group controlId="title">
                <Form.Label>{strings.email}</Form.Label>
                <Form.Control
                  type="text"
                  placeholder={strings.email}
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>
              <Button
                variant="secondary"
                type="submit"
                onClick={click ? onUpdate : onAdd}
              >
                {click ? "Save" : "Submit"}
              </Button>
            </Form>
          </Col>
          <Col md={4}>
            <img className="w-100" src={click ? img : imageURL} />
            <Form>
              <Form.Group>
                <Form.Label>Choose Image</Form.Label>
                <Form.File
                  id="img"
                  onChange={(e) => {
                    let url = URL.createObjectURL(e.target.files[0]);
                    setImageFile(e.target.files[0]);
                    setImageURL(url);
                  }}
                />
              </Form.Group>
            </Form>
          </Col>
        </Row>

        <Form>
          <div style={{ marginTop: "25px" }}>
            <Table striped bordered hover style={{ width: "100%" }}>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {author.map((item, i) => (
                  <tr>
                    <td>{item._id.slice(0, 8)}</td>
                    <td>{item.name}</td>
                    <td>{item.email}</td>
                    <td>
                      <Image
                        src={item.image}
                        rounded
                        style={{
                          objectFit: "scale-down",
                          width: "100px",
                          height: "50px",
                        }}
                      />
                    </td>
                    <td>
                      <Button
                        size="sm"
                        variant="warning"
                        onClick={() => {
                          onEdit(item._id);
                        }}
                      >
                        Edit
                      </Button>{" "}
                      <Button
                        disabled={click ? "disable" : ""}
                        size="sm"
                        variant="danger"
                        onClick={() => onDelete(item._id)}
                      >
                        Delete
                      </Button>
                    </td>
                  </tr>
                ))}{" "}
              </tbody>
            </Table>
          </div>
        </Form>
      </Col>
    </Container>
  );
}

export default Author;
