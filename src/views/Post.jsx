import React, { useState, useEffect } from "react";
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { addArt, updateArt } from "../redux/action/articleAction";
import strings from "../localization/localization";
import {
  fetchArticleById,
  updateArticleById,
  uploadImage,
} from "../services/article_service";
import { fetchCategory } from "../services/category_service";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { updateAu } from "../redux/action/authorAction";

toast.configure();
function Post() {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [imageURL, setImageURL] = useState(
    "https://designshack.net/wp-content/uploads/placeholder-image.png"
  );
  const [imageFile, setImageFile] = useState(null);
  const [categoryId, setCategoryId] = useState(null);

  const [category, setCategory] = useState([]);

  const { id } = useParams();
  const dispatch = useDispatch();

  const { message, did } = useSelector((state) => state.articleReducer);

  useEffect(() => {
    if (id) {
      fetchArticleById(id).then((article) => {
        setTitle(article.title);
        setDescription(article.description);
        setImageURL(article.image);
        setCategoryId(article.category._id);
      });
    }

    fetchCategory().then((category) => {
      setCategoryId(category[0]);
      setCategory(category);
    });

    if (did) toast.success(message);
  }, [did]);

  const onAdd = async (e) => {
    e.preventDefault();
    let article = {
      title,
      description,
      category: categoryId,
    };

    if (imageFile) {
      let url = await uploadImage(imageFile);
      article.image = url;
    }
    dispatch(addArt(article));
    setTitle("");
    setDescription("");
    setImageURL(
      "https://designshack.net/wp-content/uploads/placeholder-image.png"
    );
  };

  const onUpdate = async (e) => {
    e.preventDefault();
    let article = {
      title,
      description,
      category: categoryId,
    };
    if (imageFile) {
      let url = await uploadImage(imageFile);
      article.image = url;
    }
    // updateArticleById(id, article).then((message) => alert(message));
    dispatch(updateArt(id, article));
  };

  return (
    <Container>
      <h1 className="my-2">
        {id ? strings.updatearticle : strings.addarticle}
      </h1>
      <Row>
        <Col md={8}>
          <Form>
            <Form.Group controlId="title">
              <Form.Label>{strings.title}</Form.Label>
              <Form.Control
                type="text"
                placeholder="Title"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
              <Form.Text className="text-muted"></Form.Text>
            </Form.Group>

            <Form.Group controlId="description">
              <Form.Label>{strings.category}</Form.Label>
              <Form.Control
                as="select"
                aria-label="Choosse Category"
                onChange={(e) => setCategoryId(e.target.value)}
              >
                {category.map((category) => (
                  <option
                    key={category._id}
                    value={category._id}
                    selected={category._id === categoryId}
                  >
                    {category.name}
                  </option>
                ))}
              </Form.Control>
            </Form.Group>

            <Form.Group controlId="description">
              <Form.Label>{strings.description}</Form.Label>
              <Form.Control
                as="textarea"
                rows={4}
                placeholder="Description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>
            <Button
              variant="secondary"
              type="submit"
              onClick={id ? onUpdate : onAdd}
            >
              {id ? "Save" : "Submit"}
            </Button>
          </Form>
        </Col>
        <Col md={4}>
          <img className="w-100" src={imageURL} />
          <Form>
            <Form.Group>
              <Form.File
                id="img"
                label="Choose Image"
                onChange={(e) => {
                  let url = URL.createObjectURL(e.target.files[0]);
                  setImageFile(e.target.files[0]);
                  setImageURL(url);
                }}
              />
            </Form.Group>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}

export default Post;
