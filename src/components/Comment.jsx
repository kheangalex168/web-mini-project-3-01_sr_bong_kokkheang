import React from "react";
import { Component } from "react";

export default class Comment extends Component {
  constructor(props) {
    super(props);
    this.commentBox = React.createRef();
  }

  componentDidMount() {
    let scriptEl = document.createElement("script");
    scriptEl.setAttribute("src", "https://utteranc.es/client.js");
    scriptEl.setAttribute("repo", "KheangAlex/CommentArticle");
    scriptEl.setAttribute("crossorigin", "anonymous");
    scriptEl.setAttribute("issue-term", "url");
    scriptEl.setAttribute("theme", "github-light");
    scriptEl.setAttribute("async", true);
    this.commentBox.current.appendChild(scriptEl);
  }

  render() {
    return (
      <div>
        <div ref={this.commentBox}></div>
      </div>
    );
  }
}
