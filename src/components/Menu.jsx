import React, { useState, useContext } from "react";
import strings from "../localization/localization";
import {
  Navbar,
  Form,
  Button,
  Nav,
  FormControl,
  Container,
  NavDropdown,
} from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { LangContext } from "../context/langContext";
function Menu() {
  const [language, setLanguage] = useState([
    {
      key: "en",
      lang: "en",
    },
    {
      key: "km",
      lang: "kh",
    },
  ]);
  const context = useContext(LangContext);

  let changeLang = (lang) => {
    strings.setLanguage(lang);
    context.setLang(lang);
    localStorage.setItem("lang", lang);
  };

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#home">AMS Redux</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={NavLink} to="/">
              {strings.home}
            </Nav.Link>
            <Nav.Link as={NavLink} to="/post">
              {strings.article}
            </Nav.Link>
            <Nav.Link as={NavLink} to="/author">
              {strings.author}
            </Nav.Link>
            <Nav.Link as={NavLink} to="/category">
              {strings.category}
            </Nav.Link>
            <NavDropdown
              id="nav-dropdown-dark-example"
              title="Language"
              menuVariant="dark"
            >
              {language.map((lang, i) => (
                <NavDropdown.Item key={i} onClick={() => changeLang(lang.key)}>
                  {lang.lang}
                </NavDropdown.Item>
              ))}
            </NavDropdown>
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Menu;
