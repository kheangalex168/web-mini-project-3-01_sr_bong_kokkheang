import React, { useState, useEffect } from 'react'
import Menu from './components/Menu'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Article from './views/Article'
import Post from './views/Post'
import ViewArticle from './views/ViewArticle'
import './App.css'
import Category from './views/Category'
import Author from './views/Author'
import {LangContext} from './context/langContext'
import strings from './localization/localization'

function App() {

  const [lang, setLang] = useState()

  useEffect(() => {
      strings.setLanguage(localStorage.getItem("lang"));
  }, [])
 
  return (
    <LangContext.Provider value={{lang, setLang}}>
    <Router>
      <Menu />
      <Switch>
        <Route exact path='/' component={Article} />
        <Route path='/author' component={Author} />
        <Route path='/post' component={Post} />
        <Route path='/category' component={Category} />
        <Route path='/article/:id' component={ViewArticle} />
        <Route path='/update/article/:id' component={Post} />
        <Route path='/*' render={() => <h1>404 Not Found</h1>} />
      </Switch>
    </Router>
    </LangContext.Provider>
  )
}

export default App
