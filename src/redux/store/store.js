import { applyMiddleware, createStore } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import { combineReducers } from "redux";
import articleReducer from '../reducer/articleReducer'
import authorReducer from '../reducer/authorReducer'
import categoryReducer from '../reducer/categoryReducer'

  const rootReducer =  combineReducers({
  articleReducer,
  authorReducer,
  categoryReducer
});

  const store = createStore(rootReducer, applyMiddleware(thunk, logger))

  export default store