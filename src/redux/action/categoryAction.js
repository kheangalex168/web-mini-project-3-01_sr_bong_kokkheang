import {fetchCategory, deleteCategory, updateCategoryById, postCategory} from '../../services/category_service'

export const fetchCate = () => async dp => {
    let response = await fetchCategory()
    return dp({
        type: "FETCH_CATEGORY",
        payload: response
    })
}

export const deleteCate = (id) => async dp => {
    let response = await deleteCategory(id)
    return dp({
        type: "DELETE_CATEGORY",
        payload: response
    })
}


export const addCate = (category) => async dp => {
    let response = await postCategory(category)
    return dp({
        type: "POST_CATEGORY",
        payload: response
    })
}

export const updateCate = (id, category) => async dp => {
    let response = await updateCategoryById(id, category)
    return dp({
        type: "UPDATE_CATEGORY",
        payload: response
    })
}