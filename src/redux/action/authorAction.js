import {fetchAuthor, deleteAuthor, updateAuthorById, postAuthor} from '../../services/author_service'

export const fetchAu = () => async dp => {
    let response = await fetchAuthor()
    return dp({
        type: "FETCH_AUTHOR",
        payload: response
    })
}

export const deleteAu = (id) => async dp => {
    let response = await deleteAuthor(id)
    return dp({
        type: "DELETE_AUTHOR",
        payload: response
    })
}


export const addAu = (Author) => async dp => {
    let response = await postAuthor(Author)
    return dp({
        type: "POST_AUTHOR",
        payload: response
    })
}

export const updateAu = (id, author) => async dp => {
    let response = await updateAuthorById(id, author)
    return dp({
        type: "UPDATE_AUTHOR",
        payload: response
    })
}
