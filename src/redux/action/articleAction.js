import {fetchArticle, deleteArticle, updateArticleById, postArticle} from '../../services/article_service'
import { fetchCategory } from '../../services/category_service'


export const fetchArt = (page) => async dp => {
    let response = await fetchArticle(page)
    let category = await fetchCategory();
    return dp({
        type: "FETCH_ARTICLE",
        payload: response,
        category: category
    })
}

export const deleteArt = (id) => async dp => {
    let response = await deleteArticle(id)
    return dp({
        type: "DELETE_ARTICLE",
        payload: response
    })
}


export const addArt = (article) => async dp => {
    let response = await postArticle(article)
    return dp({
        type: "POST_ARTICLE",
        payload: response
    })
}

export const updateArt = (id, article) => async dp => {
    let response = await updateArticleById(id, article)
    return dp({
        type: "UPDATE_ARTICLE",
        payload: response
    })
}

export const fetchArtByID = (id) => async dp => {
    let response = await fetchArtByID(id)
    return dp({
        type: "FETCH_ARTICLE_BY_ID",
        payload: response
    })
}

export const load  = () => async dp => {
    return dp({
        type: "LOADING",
    })
}
