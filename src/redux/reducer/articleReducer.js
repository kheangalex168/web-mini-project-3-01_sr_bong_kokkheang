const initialState = {
    article: [],
    category: [],
    message: "",
    totalPage: null,
    loading: false,
    did: false
}

  const articleReducer = (state = initialState, { type, payload, category})  => {
    switch (type) {
    case "FETCH_ARTICLE":
        return { ...state, article: payload.data, 
            category: category , 
            totalPage: payload.total_page, 
            loading: true, did: false,  
        }

        case "DELETE_ARTICLE":
            return { ...state, did: true, message: payload}
        case "POST_ARTICLE":
            return { ...state, message: payload, did: true}
        case "UPDATE_ARTICLE":
            return { ...state, message: payload, did: true}
            case "FETCH_ARTICLE_BY_ID":
            return { ...state, message: payload }
            case "LOADING": 
            return {...state, loading: false}
    default:
        return state
    }
}

export default articleReducer

