const initialState = {
    author: [],
    did: false,
    message: ""
}

export const authorReducer = (state = initialState, { type, payload })  => {
    switch (type) {
    case "FETCH_AUTHOR":
        return { ...state, author: payload, did: false }
        case "DELETE_AUTHOR":
            return { ...state, message: payload, did: true }
        case "POST_AUTHOR":
            return { ...state, message: payload, did: true }
        case "UPDATE_AUTHOR":
            return { ...state, message: payload, did: true }

    default:
        return state
    }
}

export default authorReducer