const initialState = {
    category: [],
    did: false,
    message: ""
}

export const categoryReducer = (state = initialState, { type, payload })  => {
    switch (type) {
    case "FETCH_CATEGORY":
        return { ...state, category: payload, did: false, }
        case "DELETE_CATEGORY":
            return { ...state, message: payload, did: true }
        case "POST_CATEGORY":
            return { ...state, message: payload, did: true }
        case "UPDATE_CATEGORY":
            return { ...state, message: payload, did: true }

    default:
        return state
    }
}

export default categoryReducer