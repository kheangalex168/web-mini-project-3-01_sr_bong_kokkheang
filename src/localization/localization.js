import LocalizedStrings from 'react-localization';

let strings = new LocalizedStrings({
 en:{
    category: "Category",
    home: "Home",
    article: "Article",
    author: "Author",
    language: "Language",
    english: "English",
    khmer: "Khmer",
    updatearticle: "Update Article",
    addarticle: "Add Article",
    title: "Title",
    description: "Description",
    name : "Name",
    email: "Email"
 },
 km: {
    category: "ប្រភេទអត្ថបទ",
    home: "ទំព័រដើម",
    article: "អត្ថបទ",
    author: "អ្នកនិពន្ធ",
    language: "ភាសា",
    english: "ភាសាអង់គ្លេស",
    khmer: "ភាសាខ្មែរ",
    updatearticle: "កែប្រែអត្ថបទ",
    addarticle: "បន្ថែមអត្ថបទ",
    title: "ចំណងជើង",
    description: "ការពិពណ៏នា",
    name : "ឈ្មោះ",
    email: "អុីម៉ែល"
 }
});

export default strings